---
layout: post
section: associations
title: Grupo de usuarios/as de GNU/Linux de la Universidad de Sevilla
---

## Información

-   Universidad a la que está asociada: Universidad de Sevilla
-   Activa en la actualidad: Sí

## Contacto

-   Sede física:
-   Página web: <https://sugus.eii.us.es/>
-   Email:
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/sugus_etsii>
-   Sala en Matrix:
-   Grupo o canal en Telegram:
-   GitLab/GitHub (u otros sitios de código colaborativo):
