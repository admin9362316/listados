# Oficinas de Software Libre en las Universidades

En <https://listados.eslib.re/oficinas/> puedes encontrar listadas las páginas con información de las siguientes oficinas de software libre pertenencientes a alguna universidad:

-   [Coneixement Obert i Programari Lliure a la Universitat d’Alacant](copla-ua.md)
-   [Consorcio para o Desenvolvemento de Aplicacións de Xestión Universitaria](osl-cixug.md)
-   [ITSAS - Grupo de Software Libre de la Universidad del País Vasco/Euskal Herriko Unibertsitatea](itsas-upv-ehu.md)
-   [Oficina de Conocimiento y Cultura Libres de la Universidad Rey Juan Carlos](ofilibre-urjc.md)
-   [Oficina de Software Libre de la Universidad Carlos III de Madrid](osl-uc3m.md)
-   [Oficina de Software Libre de la Universidad Complutense de Madrid](osl-ucm.md)
-   [Oficina de Software Libre de la Universidad de Granada](osl-ugr.md)
-   [Oficina de Software Libre de la Universidad de La Laguna](osl-ull.md)
-   [Oficina de Software Libre de la Universidad de Las Palmas de Gran Canaria](osl-ulpgc.md)
-   [Oficina de Software Libre de la Universidad de Zaragoza](osl-uz.md)
-   [Oficina de Software Libre y Conocimiento Abierto de la Universidad de Cádiz](osl-uca.md)
-   [Oficina de Software Libre y Conocimiento Abierto de la Universidad de Huelva](osl-uhu.md)
-   [Oficina de Software y Hardware Libre de la Universidad Miguel Hernández](oslh-umh.md)
-   [Oficina per al Programari Lliure de la Universitat Autònoma de Barcelona](opl-uab.md)
-   [Software Libre y Abierto en la Universidad de Murcia](softla-um.md)
-   [Software Libre y Fuentes Abiertas en la Universidad de Sevilla](solfa-us.md)

Si conoces alguna que no aparece aquí puedes añadirla directamente en este mismo archivo, pero si puedes, te agradeceríamos que añadieras en esta misma carpeta su página de información usando [esta plantilla](../plantillas/oficinas.md).
